package br.com.justo.worktime.usecases

import br.com.justo.worktime.gateway.documents.Company

interface CompanyManager {

    fun findByCnpj(cnpj: String): Company?

    fun save(company: Company): Company

}