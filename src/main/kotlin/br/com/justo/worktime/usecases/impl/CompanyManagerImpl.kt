package br.com.justo.worktime.usecases.impl

import br.com.justo.worktime.gateway.db.CompanyRepository
import br.com.justo.worktime.gateway.documents.Company
import br.com.justo.worktime.usecases.CompanyManager
import org.springframework.stereotype.Component

@Component
class CompanyManagerImpl(val companyRepository: CompanyRepository) : CompanyManager {

    override fun findByCnpj(cnpj: String): Company? = companyRepository.findByCnpj(cnpj)

    override fun save(company: Company): Company = companyRepository.save(company)

}