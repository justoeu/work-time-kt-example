package br.com.justo.worktime.usecases.impl

import br.com.justo.worktime.gateway.db.EmployerRepository
import br.com.justo.worktime.gateway.documents.Employer
import br.com.justo.worktime.usecases.EmployerManager
import org.springframework.stereotype.Component
import java.util.*

@Component
class EmployerManagerImpl(val respository: EmployerRepository) : EmployerManager {



    override fun findByCpf(cpf: String): Employer? = respository?.findByCpf(cpf)

    override fun findByEmail(email: String): Employer? = respository?.findByEmail(email)

    override fun findById(id: String): Employer? {
        val empl : Optional<Employer> = respository?.findById(id)

        return empl.get()
    }

    override fun save(employer: Employer): Employer = respository?.save(employer)
}