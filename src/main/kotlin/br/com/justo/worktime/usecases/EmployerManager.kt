package br.com.justo.worktime.usecases

import br.com.justo.worktime.gateway.documents.Employer

interface EmployerManager {


    fun findByCpf(cpf: String): Employer?
    fun findByEmail(email: String): Employer?
    fun findById(id: String): Employer?
    fun save(employer: Employer): Employer


}