package br.com.justo.worktime.utils

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

class PasswordUtils {

    fun generatePass(pass: String): String = BCryptPasswordEncoder().encode(pass)
}