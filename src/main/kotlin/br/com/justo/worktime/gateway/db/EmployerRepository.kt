package br.com.justo.worktime.gateway.db

import br.com.justo.worktime.gateway.documents.Employer
import org.springframework.data.mongodb.repository.MongoRepository

interface EmployerRepository : MongoRepository<Employer, String> {

    fun findByEmail(email: String): Employer
    fun findByCpf(cpf: String): Employer

}