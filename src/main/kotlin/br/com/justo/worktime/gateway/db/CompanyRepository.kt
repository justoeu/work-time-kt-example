package br.com.justo.worktime.gateway.db

import br.com.justo.worktime.gateway.documents.Company
import org.springframework.data.mongodb.repository.MongoRepository

interface CompanyRepository : MongoRepository<Company, String> {

    fun findByCnpj(cnpj: String) : Company

}