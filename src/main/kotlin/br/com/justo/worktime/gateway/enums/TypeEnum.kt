package br.com.justo.worktime.gateway.enums

enum class TypeEnum {

    START_WORK,
    END_WORK,
    START_LUNCH,
    END_LUNCH,
    START_PAUSE,
    END_PAUSE;

}