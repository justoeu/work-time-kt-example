package br.com.justo.worktime.gateway.db

import br.com.justo.worktime.gateway.documents.Launch
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository

interface EntriesRepository : MongoRepository<Launch, String> {

    fun findByEmployeId(employeId: String, pageable: Pageable): Page<Launch>

}