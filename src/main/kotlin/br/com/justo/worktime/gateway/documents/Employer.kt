package br.com.justo.worktime.gateway.documents

import br.com.justo.worktime.gateway.enums.PerfilEnum
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Employer (

        @Id val id: String? = null,
        val name: String,
        val cpf: String,
        val email: String,
        val pass: String,
        val perfil: PerfilEnum,
        val companyID: String?,
        val moneyTime: Double? = 0.0,
        val qtdTimeWorkDay: Float? = 0.0f,
        val qtdTimeLunch: Float? = 0.0f
)
