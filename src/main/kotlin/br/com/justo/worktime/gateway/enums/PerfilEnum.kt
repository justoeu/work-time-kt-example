package br.com.justo.worktime.gateway.enums

enum class PerfilEnum {
    ROLE_ADMIN,
    ROLE_USER;
}