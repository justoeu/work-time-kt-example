package br.com.justo.worktime.gateway.documents

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Company (

    // var é variavel
    // val é constante
    // Acabei deixando como val pq todos os campos, nesse caso, sao obrigatorios
        // o data representa os Gets e Sets toString, Equal, etc

    @Id val id: String? = null,
    val razaoSocial: String,
    val cnpj: String

)