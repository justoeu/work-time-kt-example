package br.com.justo.worktime.gateway.documents

import br.com.justo.worktime.gateway.enums.TypeEnum
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document
data class Launch (

        @Id val id: String? = null,
        val date: Date,
        val type: TypeEnum,
        val employeId: String,
        val description: String? = "",
        val locate: String? = ""

)
