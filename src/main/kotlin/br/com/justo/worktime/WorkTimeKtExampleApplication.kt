package br.com.justo.worktime

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WorkTimeKtExampleApplication

fun main(args: Array<String>) {
    runApplication<WorkTimeKtExampleApplication>(*args)
}
