package br.com.justo.worktime.utils

import org.junit.Assert
import org.junit.Test
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

class PasswordUtilsTest {

    private val pass = "123456"
    private val bCrpt = BCryptPasswordEncoder()


    @Test
    fun test_generateHashPassword(){

        val hash = PasswordUtils().generatePass(pass)
        Assert.assertTrue(bCrpt.matches(pass,hash))

    }

}