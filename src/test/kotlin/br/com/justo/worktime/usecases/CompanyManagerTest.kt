package br.com.justo.worktime.usecases

import br.com.justo.worktime.gateway.db.CompanyRepository
import br.com.justo.worktime.gateway.documents.Company
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureDataMongo
class CompanyManagerTest {

    @Autowired
    val companyManager: CompanyManager? = null

    @MockBean
    private val repository: CompanyRepository? = null

    private val CNPJ = "10232972000165"

    @Before
    @Throws(Exception::class)
    fun setUp(){
        BDDMockito.given(repository?.findByCnpj(CNPJ)).willReturn(companyFake())
        BDDMockito.given(repository?.save(companyFake())).willReturn(companyFake())
    }

    @Test
    fun test_findCompanyByCnpjWithCompanyValid_shouldReturnCompany(){

        val company: Company? = companyManager?.findByCnpj(CNPJ)
        Assert.assertNotNull(company)

    }

    @Test
    fun test_findCompanyByCnpjWithCompanyNotValid_shouldReturnNull(){

        BDDMockito.given(repository?.findByCnpj("123456")).willReturn(null)
        val company: Company? = companyManager?.findByCnpj("123456")

        Assert.assertNull(company)
    }

    @Test
    fun test_saveCompanyWithCompanyValid_shouldReturnSameCompany(){

        val newCompany: Company? = companyManager?.save(companyFake())
        Assert.assertNotNull(newCompany)
        Assert.assertEquals(newCompany?.cnpj, CNPJ)

    }

    @Test(expected = Exception::class)
    fun test_saveCompanyWithCompanyNotValid_shouldReturnException(){
        BDDMockito.given(repository?.save(BDDMockito.any<Company>())).willThrow(Exception())

        companyManager?.save(companyFake())
    }


    private fun companyFake(): Company = (Company("1", "FakeCompany", CNPJ))
}