package br.com.justo.worktime.usecases

import br.com.justo.worktime.gateway.db.EmployerRepository
import br.com.justo.worktime.gateway.documents.Employer
import br.com.justo.worktime.gateway.enums.PerfilEnum
import br.com.justo.worktime.utils.PasswordUtils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class EmployerManagerTest {

    @MockBean
    private val repository: EmployerRepository? = null

    @Autowired
    private  val employerManager: EmployerManager? = null

    private val email: String = "teste@teste.com"
    private val cpf: String = "22266699933"
    private val id: String = "1"
    private val idCompany: String = "1"
    private val pass: String = "123456"


    @Before
    @Throws(Exception::class)
    fun setUp(){

        BDDMockito.given(repository?.findByCpf(cpf)).willReturn(createEmployer())
        BDDMockito.given(repository?.findById(id)).willReturn(Optional.of(createEmployer()))
        BDDMockito.given(repository?.findByEmail(email)).willReturn(createEmployer())
        BDDMockito.given(repository?.save(Mockito.any(Employer::class.java))).willReturn(createEmployer())
        //BDDMockito.given(repository?.save(BDDMockito.any<Employer>())).willReturn(createEmployer())

    }

    @Test
    fun test_saveEmployerWithEmployerValid_shouldReturnEmployer(){

        val employer: Employer? = employerManager?.save(createEmployer())

        Assert.assertNotNull(employer)
        Assert.assertEquals(employer?.name, "Teste")
    }

    @Test
    fun test_findEmployerUsingCpfParameterValid_shouldReturnEmployer(){

        val employer : Employer? = employerManager?.findByCpf(cpf)

        Assert.assertNotNull(employer)
        Assert.assertEquals(employer?.cpf, cpf)
    }

    @Test
    fun test_findEmployerUsingCpfParameterNotValid_shouldReturnEmployer(){

        BDDMockito.given(repository?.findByCpf("123")).willReturn(null)
        val employer : Employer? = employerManager?.findByCpf("123")

        Assert.assertNull(employer)
    }

    @Test
    fun test_findEmployerUsingEmailParameterValid_shouldReturnEmployer(){

        val employer : Employer? = employerManager?.findByEmail(email)

        Assert.assertNotNull(employer)
        Assert.assertEquals(employer?.email, email)
    }

    @Test
    fun test_findEmployerUsingEmailParameterNotValid_shouldReturnEmployer(){
        BDDMockito.given(repository?.findByEmail("teste@xxx.com")).willReturn(null)
        val employer : Employer? = employerManager?.findByCpf("teste@xxx.com")

        Assert.assertNull(employer)
    }

    private fun createEmployer(): Employer = Employer(null,"Teste", cpf, email, PasswordUtils().generatePass(pass), PerfilEnum.ROLE_USER ,idCompany)

}